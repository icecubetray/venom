CC = gcc
LD = gcc
ARCH = x64

CFLAGS = -Wall -fPIC -fstack-protector-strong -I. -DMAKEFILE=1


default: release


clean:
	@rm -Rf bin
	@rm -Rf obj


obj/$(ARCH)/%.o: %.c
	@mkdir -p $(@D)
	@$(CC) $(CFLAGS) -c $^ -o $@
	@echo "| $@"

bin/libvenom.so: CFLAGS += -DLV_EXPORTING=1
bin/libvenom.so: $(addprefix obj/$(ARCH)/, $(patsubst %.c, %.o, $(shell find libvenom -type f -name '*.c')))
	@mkdir -p $(@D)
	@$(LD) -o $@ -shared $^
	@echo "+-> $@"
	@echo

bin/test: CFLAGS += -g -DDEBUG=1
bin/test: $(addprefix obj/$(ARCH)/, $(patsubst %.c, %.o, $(shell find libvenomtest -type f -name '*.c')))
bin/test: bin/libvenom.so
	@mkdir -p $(@D)
	@$(LD) -o $@ -L./bin -lvenom $^
	@echo "+-> $@"
	@echo

release: CFLAGS += -O3 -DRELEASE=1
release: bin/libvenom.so

debug: bin/test
