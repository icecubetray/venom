﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetVenom.Runtime;
using System.Threading;

namespace netvenomtest
{
	class Program
	{
		static void Main(string[] args) {
			VenomProcess process;
			while ((process = VenomProcess.AwaitWindowSecs((ProcessFlags.MemoryReadWrite | ProcessFlags.Suspendable), "Command & Conquer Generals", 1, 10)) == null) {
				Console.WriteLine("Waited 10 seconds for process to show up.");
			}
			Console.WriteLine("Found it!");

			process.Exited += (proc, code) => {
				Console.WriteLine("Booyah! " + proc.Identifier.ToString() + " exited with exit code " + code.ToString() + ".");
			};

			bool susp = true;
			while (Console.ReadLine().ToLower() != "q") {
				if (susp) {
					process.Suspend();
				} else {
					process.Resume();
				}
				susp = !susp;
			}

			process.Dispose();
		}
	}
}
