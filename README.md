# Venom
Process manipulation library.

## Projects
- [libvenom (main)](./libvenom)
- [libvenomtest (test: C)](./libvenomtest)
- [NetVenom (binding: C#)](./netvenom)
- [netvenomtest (test: C#)](./netvenomtest)
