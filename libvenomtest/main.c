#include <stdio.h>

#include <libvenom/core/time.h>
#include <libvenom/runtime/process.h>


void exit_callback(const lv_process_t *const process) {
}


int main(int argc, char *argv[], char *env[]) {
	printf("RDTSC: %llu\n", lv_rdtsc());
	return 0;

	lv_process_t process;
	while (lv_process_open_ex(&process, LV_PROCESS_MREADWRITE|LV_PROCESS_SUSPENDABLE, 0, "Command & Conquer Generals", exit_callback) != 0) {//lv_process_open_await_window(&process, LV_PROCESS_MREADWRITE | LV_PROCESS_SUSPENDABLE, "Command & Conquer Generals", 500000000, 10000000000) != 0) {
		puts("Awaiting process...");
		lv_sleep_nanos(1000000000);
	}
	puts("Gotcha!");

	/*if (lv_process_register_exit_callback(&process, exit_callback) != 0) {
		puts("Uh-oh...");
	}*/

	puts("Awaiting keystroke...");
	fgetc(stdin);

	lv_process_close(&process);

	return 0;
}
