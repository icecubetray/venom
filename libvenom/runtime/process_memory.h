/*******************************************************************************
**                                                                            **
**   The MIT License                                                          **
**                                                                            **
**   Copyright 2017 icecubetray                                               **
**                                                                            **
**   Permission is hereby granted, free of charge, to any person              **
**   obtaining a copy of this software and associated documentation files     **
**   (the "Software"), to deal in the Software without restriction,           **
**   including without limitation the rights to use, copy, modify, merge,     **
**   publish, distribute, sublicense, and/or sell copies of the Software,     **
**   and to permit persons to whom the Software is furnished to do so,        **
**   subject to the following conditions:                                     **
**                                                                            **
**   The above copyright notice and this permission notice shall be           **
**   included in all copies or substantial portions of the Software.          **
**                                                                            **
**   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          **
**   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       **
**   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   **
**   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY     **
**   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,     **
**   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        **
**   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                   **
**                                                                            **
********************************************************************************
**
**  Notes:
**    -
**
*/

#ifndef __LV_RUNTIME_PROCESS_MEMORY_H
#define __LV_RUNTIME_PROCESS_MEMORY_H




#include "./process.h"




#ifdef __cplusplus
extern "C" {
#endif


LVAPI
lv_result_t lv_process_memr(
	__IN	const lv_process_t *const restrict process,
	__IN	const void *const restrict address,
	__OUT	void *const restrict buffer,
	__IN	const size_t nbytes,
	__OUT	size_t *const restrict nread
);

LVAPI
lv_result_t lv_process_memw(
	__IN	const lv_process_t *const restrict process,
	__IN	void *const restrict address,
	__IN	const void *const restrict buffer,
	__IN	const size_t nbytes,
	__OUT	size_t *const restrict nwritten
);

LVAPI
lv_result_t lv_process_memr16(
	__IN	const lv_process_t *const restrict process,
	__IN	const void *const restrict address,
	__OUT	uint16_t *const restrict value
);

LVAPI
lv_result_t lv_process_memw16(
	__IN	const lv_process_t *const restrict process,
	__IN	void *const restrict address,
	__IN	const uint16_t value
);

LVAPI
lv_result_t lv_process_memr32(
	__IN	const lv_process_t *const restrict process,
	__IN	const void *const restrict address,
	__OUT	uint32_t *const restrict value
);

LVAPI
lv_result_t lv_process_memw32(
	__IN	const lv_process_t *const restrict process,
	__IN	void *const restrict address,
	__IN	const uint32_t value
);

LVAPI
lv_result_t lv_process_memr64(
	__IN	const lv_process_t *const restrict process,
	__IN	const void *const restrict address,
	__OUT	uint64_t *const restrict value
);

LVAPI
lv_result_t lv_process_memw64(
	__IN	const lv_process_t *const restrict process,
	__IN	void *const restrict address,
	__IN	const uint64_t value
);

LVAPI
lv_result_t lv_process_memderef_ex(
	__IN	const lv_process_t *const restrict process,
	__IN	const void *const *const restrict address,
	__IN	const size_t offset,
	__OUT	const void **const restrict value,
	__IN	const uint_fast8_t ptr_size
);

LVAPI
lv_result_t lv_process_memderef(
	__IN	const lv_process_t *const restrict process,
	__IN	const void *const *const restrict address,
	__IN	const size_t offset,
	__OUT	const void **const restrict value
);

LVAPI
lv_result_t lv_process_memderefmul_ex(
	__IN	const lv_process_t *const restrict process,
	__IN	const void *const *const restrict address,
	__IN	const size_t *const restrict offsets,
	__IN	const uint32_t noffsets,
	__OUT	const void **const restrict value,
	__IN	const uint_fast8_t ptr_size
);

LVAPI
lv_result_t lv_process_memderefmul(
	__IN	const lv_process_t *const restrict process,
	__IN	const void *const *const restrict address,
	__IN	const size_t *const restrict offsets,
	__IN	const uint32_t noffsets,
	__OUT	const void **const restrict value
);


#ifdef __cplusplus
}
#endif




#endif
