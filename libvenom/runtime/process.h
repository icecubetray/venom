/*******************************************************************************
**                                                                            **
**   The MIT License                                                          **
**                                                                            **
**   Copyright 2017 icecubetray                                               **
**                                                                            **
**   Permission is hereby granted, free of charge, to any person              **
**   obtaining a copy of this software and associated documentation files     **
**   (the "Software"), to deal in the Software without restriction,           **
**   including without limitation the rights to use, copy, modify, merge,     **
**   publish, distribute, sublicense, and/or sell copies of the Software,     **
**   and to permit persons to whom the Software is furnished to do so,        **
**   subject to the following conditions:                                     **
**                                                                            **
**   The above copyright notice and this permission notice shall be           **
**   included in all copies or substantial portions of the Software.          **
**                                                                            **
**   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          **
**   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       **
**   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   **
**   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY     **
**   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,     **
**   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        **
**   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                   **
**                                                                            **
********************************************************************************
**
**  Notes:
**    -
**
*/

#ifndef __LV_RUNTIME_PROCESS_H
#define __LV_RUNTIME_PROCESS_H




#include "../core/stdincl.h"
#include "../core/platform.h"




#define __LV_PROCESS_MAGIC					0xA929

#define LV_PROCESS_RUNNING					0x0100
#define LV_PROCESS_32BIT					0x0200
#define LV_PROCESS_64BIT					0x0400
#define LV_PROCESS_REGISTERED_EXIT			0x0800
#define LV_PROCESS_SUSPENDED				0x1000
#define LV_PROCESS_TERMINATED				0x2000

#define LV_PROCESS_MREAD					0x0001
#define LV_PROCESS_MWRITE					0x0002
#define LV_PROCESS_MREADWRITE				(LV_PROCESS_MREAD | LV_PROCESS_MWRITE)
#define LV_PROCESS_REGISTER_EXIT			0x0004
#define LV_PROCESS_SUSPENDABLE				0x0008
#define LV_PROCESS_TERMINATABLE				0x0010

#define LV_PROCESS_USER_FLAGS				(LV_PROCESS_MREADWRITE | LV_PROCESS_REGISTER_EXIT | LV_PROCESS_SUSPENDABLE | LV_PROCESS_TERMINATABLE)

#define LV_PROCESS_ERROR_EXIT_CODE			0x8000




#define LV_IS_PROCESS_RUNNING(processPtr)	LV_FLAG(((lv_process_t*)(processPtr))->flags, LV_PROCESS_RUNNING)




typedef struct lv_process lv_process_t;
typedef void(*lv_process_exit_callback)(const lv_process_t *const process);
struct lv_process {
	lv_process_exit_callback exit_callback;
#if (LV_WINDOWS)
	HANDLE __handle;
	HANDLE __wait_handle;
#endif
	lv_pid_t identifier;
	uint32_t exit_code;
	lv_flags_t flags;
	uint16_t __magic;
};

// NOTE: When adding members, ensure they are written to in lv_process_open_ex
//       and zeroed out in lv_process_close.




#ifdef __cplusplus
extern "C" {
#endif


LVAPI
lv_result_t lv_process_open_ex(
	__OUT			lv_process_t *const restrict process,
	__IN			lv_flags_t flags,
	__IN_EITHER(1)	lv_pid_t identifier,
	__IN_EITHER(1)	const char *const restrict wndText,
	__IN			const lv_process_exit_callback exit_callback
) non_null(1);

LVAPI
lv_result_t lv_process_open(
	__OUT			lv_process_t *const restrict process,
	__IN			const lv_flags_t flags,
	__IN_EITHER(1)	const lv_pid_t identifier,
	__IN_EITHER(1)	const char *const restrict wndText
);

LVAPI
lv_result_t lv_process_open_await_window(
	__OUT			lv_process_t *const restrict process,
	__IN			const lv_flags_t flags,
	__IN			const char *const restrict wnd_text,
	__IN			const uint64_t ns_interval,
	__IN			const uint64_t ns_timeout
);

LVAPI
lv_result_t lv_process_close(
	__IN			lv_process_t *const process
);

LVAPI
lv_result_t lv_process_register_exit_callback(
	__IN			lv_process_t *const process,
	__IN			const lv_process_exit_callback exit_callback
);

LVAPI
lv_result_t lv_process_unregister_exit_callback(
	__IN			lv_process_t *const process
);

LVAPI
lv_result_t lv_process_suspend(
	__IN			const lv_process_t *const process
);

LVAPI
lv_result_t lv_process_resume(
	__IN			const lv_process_t *const process
);

/*LVAPI
lv_result_t lv_process_get_suspended(
	__IN			const lv_process_t *const process
);*/

LVAPI
lv_result_t lv_process_terminate(
	__IN			const lv_process_t *const process,
	__IN			const uint32_t exit_code
);


#ifdef __cplusplus
}
#endif




#endif
