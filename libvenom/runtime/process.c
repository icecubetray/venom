/*******************************************************************************
**                                                                            **
**   The MIT License                                                          **
**                                                                            **
**   Copyright 2017 icecubetray                                               **
**                                                                            **
**   Permission is hereby granted, free of charge, to any person              **
**   obtaining a copy of this software and associated documentation files     **
**   (the "Software"), to deal in the Software without restriction,           **
**   including without limitation the rights to use, copy, modify, merge,     **
**   publish, distribute, sublicense, and/or sell copies of the Software,     **
**   and to permit persons to whom the Software is furnished to do so,        **
**   subject to the following conditions:                                     **
**                                                                            **
**   The above copyright notice and this permission notice shall be           **
**   included in all copies or substantial portions of the Software.          **
**                                                                            **
**   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          **
**   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       **
**   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   **
**   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY     **
**   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,     **
**   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        **
**   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                   **
**                                                                            **
********************************************************************************
**
**  Notes:
**    -
**
*/

#include "./process.h"
#include "../core/time.h"




/*
**	Conditional compilation options:
**
**		LV_PROCESS_DIE_OPEN_IMAGE_TYPE_FAIL
**			Cleans up and returns an error if the lv_process_open[_ex] function
**			is unable to determine what type of image (32-bit, 64-bit) the
**			target process is.
**			If it is not specified, failure to determine the image type will
**			leave both LV_PROCESS_32BIT and LV_PROCESS_64BIT unspecified and
**			will cause lv_process_memderef[mul] to fail because it will be
**			unable to determine the size of the pointers the target process is
**			using. Using lv_process_memderef[mul]_ex will fix this issue since
**			the pointer size is specified manually.
**
**		LV_PROCESS_DIE_OPEN_REGISTER_FAIL
**			Cleans up and returns an error if exit callback registration fails
**			in the lv_process_open[_ex] function.
**			If it is not specified, the callback failure is ignored and the
**          process remains as if no callback was requested, but the function
**			returns with the subroutine flag set to inform the caller about the
**			failure.
**
*/




#if (LV_WINDOWS)
int sysInfo_initialized = 0, proc_initialized = 0;
SYSTEM_INFO sysInfo = { 0 };

typedef LONG(NTAPI *NtProcessProc)(IN HANDLE hProcess);
HMODULE ntdll = NULL;
NtProcessProc NtSuspendProcess = NULL, NtResumeProcess = NULL;




static void CALLBACK __lv_process_exit_callback(lv_process_t *const process, BOOLEAN bTimerOrWaitFired) {
	if (!process) {
		log("Parameter for process exit callback is nulled.");
		return;
	}

	if (process->__magic != __LV_PROCESS_MAGIC) {
		logf("Process structure passed to process exit callback has an invalid magic: %016llX %04X", (unsigned long long)(uintptr_t)process, process->__magic);
		return;
	}

	lv_flags_t additional_flags = 0;

	if (!GetExitCodeProcess(process->__handle, (LPDWORD)&process->exit_code)) {
		additional_flags |= LV_PROCESS_ERROR_EXIT_CODE;
	}

	process->flags = ((process->flags | additional_flags) & ~LV_PROCESS_RUNNING);

	if (process->exit_callback) {
		process->exit_callback(process);
	}
}


static inline lv_result_t __lv_process_suspend_proc_init() {
	if (!proc_initialized) {
		ntdll = GetModuleHandle("ntdll");
		if (!ntdll) {
			return 1;
		}

		NtSuspendProcess = (NtProcessProc)GetProcAddress(ntdll, "NtSuspendProcess");
		if (!NtSuspendProcess) {
			return 2;
		}

		NtResumeProcess = (NtProcessProc)GetProcAddress(ntdll, "NtResumeProcess");
		if (!NtResumeProcess) {
			return 3;
		}

		proc_initialized = 1;
	}

	return 0;
}
#endif


static lv_result_t __lv_process_register_exit_callback(lv_process_t *const restrict process, const lv_process_exit_callback exit_callback, lv_flags_t *const restrict flags) {
	if (!exit_callback) {
		return LV_RESULT_NULLPOINTER;
	}

	if (LV_FLAG(*flags, LV_PROCESS_REGISTERED_EXIT)) {
		return 1;
	}

#if (LV_WINDOWS)
	if (RegisterWaitForSingleObject(&process->__wait_handle, process->__handle, __lv_process_exit_callback, process, INFINITE, WT_EXECUTEONLYONCE)) {
		*flags |= LV_PROCESS_REGISTERED_EXIT;
		process->exit_callback = exit_callback;
		return LV_RESULT_SUCCESS;
	} else {
		return LV_RESULT_SUBROUTINE_FAILED;
	}
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif

	return LV_RESULT_UNEXP_TERMINATION;
}




lv_result_t lv_process_open_ex(lv_process_t *const restrict process, lv_flags_t flags, lv_pid_t identifier, const char *const restrict wnd_text, const lv_process_exit_callback exit_callback) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!(!identifier ^ !wnd_text)) {
		log(LV_STR_PARAM_ERROR LV_STR_SEPARATOR "xor test failed on parameters identifier and wnd_text");
		return LV_RESULT_PARAMETER_INVALID;
	}

	if (wnd_text) {
#if (LV_WINDOWS)
		HWND hWnd = FindWindow(NULL, wnd_text);
		if (!hWnd) {
			return 1;
		}

		GetWindowThreadProcessId(hWnd, &identifier);
		if (!identifier) {
			return 2;
		}
#else
		__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif
	}

	if (identifier) {
#if (LV_WINDOWS)
		DWORD access = 0;

		if (LV_FLAG(flags, LV_PROCESS_MREAD)) {
			access |= (PROCESS_VM_OPERATION | PROCESS_VM_READ);
		}
		if (LV_FLAG(flags, LV_PROCESS_MWRITE)) {
			access |= (PROCESS_VM_OPERATION | PROCESS_VM_WRITE);
		}
		if (LV_FLAG(flags, LV_PROCESS_REGISTER_EXIT)) {
			access |= SYNCHRONIZE;
		}
		if (LV_FLAG(flags, LV_PROCESS_SUSPENDABLE)) {
			access |= PROCESS_SUSPEND_RESUME;
		}
		if (LV_FLAG(flags, LV_PROCESS_TERMINATABLE)) {
			access |= PROCESS_TERMINATE;
		}

		HANDLE hProc = OpenProcess(access, 0, identifier);
		if (!hProc) {
			return 1;
		}

		BOOL wow64 = 0;
		if (!IsWow64Process(hProc, &wow64)) {
			logf("IsWow64Process(...) failed: GetLastError = %08X", GetLastError());
#ifdef LV_PROCESS_DIE_OPEN_IMAGE_TYPE_FAIL
			CloseHandle(hProc);
			return LV_RESULT_SUBROUTINE_FAILED;
#else
			wow64 = 2;
#endif
		}

		flags = ((flags & LV_PROCESS_USER_FLAGS) | LV_PROCESS_RUNNING);

		if (wow64 == 1) {
			flags |= LV_PROCESS_32BIT;
		} else if (wow64 == 0) {
			if (!sysInfo_initialized) {
				sysInfo_initialized = 1;
				GetNativeSystemInfo(&sysInfo);
			}

			if (sysInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64 || sysInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64) {
				flags |= LV_PROCESS_64BIT;
			}
		}

		process->__magic = __LV_PROCESS_MAGIC;
		process->__handle = hProc;

		lv_result_t result = LV_RESULT_SUCCESS;

		if (LV_FLAG(flags, LV_PROCESS_REGISTER_EXIT) && exit_callback) {
			if ((result = __lv_process_register_exit_callback(process, exit_callback, &flags)) != 0) {
				logf("Failed to register exit callback: result = %u", result);
#ifdef LV_PROCESS_DIE_OPEN_REGISTER_FAIL
				CloseHandle(hProc);
				return (LV_RESULT_FLAG_SUBROUTINE | result);
#else
				result = (LV_RESULT_FLAG_SUBROUTINE | LV_RESULT_SUCCESS);
#endif
			}
		}

		process->identifier = identifier;
		process->exit_code = 0;
		process->flags = flags;

		return result;
#else
		__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif
	}

	return LV_RESULT_UNEXP_TERMINATION;
}

lv_result_t lv_process_open(lv_process_t *const restrict process, const lv_flags_t flags, const lv_pid_t identifier, const char *const restrict wnd_text) {
	return lv_process_open_ex(process, flags, identifier, wnd_text, NULL);
}

lv_result_t lv_process_open_await_window(lv_process_t *const restrict process, const lv_flags_t flags, const char *const restrict wnd_text, const uint64_t ns_interval, const uint64_t ns_timeout) {
	if (!process || !wnd_text) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

#if (LV_WINDOWS)
	uint64_t ns_start = lv_nanos();

	HWND hwnd = FindWindow(NULL, wnd_text);
	while (!hwnd) {
		if (ns_timeout && ((lv_nanos() - ns_start) > ns_timeout)) {
			return LV_RESULT_TIMEOUT;
		}

		lv_sleep_nanos(ns_interval);

		hwnd = FindWindow(NULL, wnd_text);
	}

	lv_pid_t identifier = 0;
	GetWindowThreadProcessId(hwnd, &identifier);
	if (!identifier) {
		return 2;
	}

	return lv_process_open(process, flags, identifier, NULL);
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif
}


lv_result_t lv_process_close(lv_process_t *const process) {
	if (!process) {
		log(LV_STR_NULLPOINTER);

		return LV_RESULT_NULLPOINTER;
	}

	lv_result_t result;
	if ((result = lv_process_unregister_exit_callback(process)) != 0) {
		return (LV_RESULT_FLAG_SUBROUTINE | result);
	}

#if (LV_WINDOWS)
	if (process->__handle) {
		if (CloseHandle(process->__handle)) {
			process->__handle = NULL;
		} else {
			return LV_RESULT_SUBROUTINE_FAILED;
		}
	}
#endif

	process->identifier = 0;
	process->exit_code = 0;
	process->flags = 0;

	process->__magic = 0;
	return LV_RESULT_SUCCESS;
}




lv_result_t lv_process_register_exit_callback(lv_process_t *const process, const lv_process_exit_callback exit_callback) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	return __lv_process_register_exit_callback(process, exit_callback, &process->flags);
}


lv_result_t lv_process_unregister_exit_callback(lv_process_t *const process) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

#if (LV_WINDOWS)
	if (process->__wait_handle && LV_FLAG(process->flags, LV_PROCESS_REGISTERED_EXIT)) {
		if (UnregisterWait(process->__wait_handle)) {
			process->__wait_handle = NULL;
			process->flags &= ~LV_PROCESS_REGISTERED_EXIT;

			return LV_RESULT_SUCCESS;
		} else {
			return LV_RESULT_SUBROUTINE_FAILED;
		}
	} else {
		return LV_RESULT_INVALID_STATE;
	}
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif

	return LV_RESULT_UNEXP_TERMINATION;
}




lv_result_t lv_process_suspend(const lv_process_t *const process) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!LV_FLAG(process->flags, LV_PROCESS_SUSPENDABLE)) {
		log("Process is not specified as suspendable.");
		return LV_RESULT_INVALID_FLAGS;
	}

	if (!LV_IS_PROCESS_RUNNING(process)) {
		log("Process has to be running in order to be suspended.");
		return LV_RESULT_INVALID_STATE;
	}

#if (LV_WINDOWS)
	if (!proc_initialized && (__lv_process_suspend_proc_init() != 0)) {
		return LV_RESULT_INIT_FAILED;
	}

	NtSuspendProcess(process->__handle);

	return 0;
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif

	return LV_RESULT_UNEXP_TERMINATION;
}


lv_result_t lv_process_resume(const lv_process_t *const process) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!LV_FLAG(process->flags, LV_PROCESS_SUSPENDABLE)) {
		log("Process is not specified as suspendable.");
		return LV_RESULT_INVALID_FLAGS;
	}

	if (!LV_IS_PROCESS_RUNNING(process)) {
		log("Process has to be running in order to be resumed.");
		return LV_RESULT_INVALID_STATE;
	}

#if (LV_WINDOWS)
	if (!proc_initialized && (__lv_process_suspend_proc_init() != 0)) {
		return LV_RESULT_INIT_FAILED;
	}

	NtResumeProcess(process->__handle);

	return 0;
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif

	return LV_RESULT_UNEXP_TERMINATION;
}


/*lv_result_t lv_process_get_suspended(const lv_process_t *const process) {
	/*
	**
	**	EXPERIMENTAL, FIXME!
	**
	* /

	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	// checks

#if (LV_WINDOWS)
	FILETIME ct1, et1, kt1, ut1;
	GetProcessTimes(process->__handle, &ct1, &et1, &kt1, &ut1);

	Sleep(100);

	FILETIME ct2, et2, kt2, ut2;
	GetProcessTimes(process->__handle, &ct2, &et2, &kt2, &ut2);

	if (memcmp(&ct1, &ct2, sizeof(ct1)) != 0) {
		return 1;
	}

	if (memcmp(&et1, &et2, sizeof(et1)) != 0) {
		return 2;
	}

	if (memcmp(&kt1, &kt2, sizeof(kt1)) != 0) {
		return 3;
	}

	if (memcmp(&ut1, &ut2, sizeof(ut1)) != 0) {
		return 4;
	}

	return 0;
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif

	return LV_RESULT_UNEXP_TERMINATION;
}*/


lv_result_t lv_process_terminate(const lv_process_t *const process, const uint32_t exit_code) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!LV_FLAG(process->flags, LV_PROCESS_TERMINATABLE)) {
		log("Process is not specified as terminatable.");
		return LV_RESULT_INVALID_FLAGS;
	}

	if (!LV_IS_PROCESS_RUNNING(process)) {
		log("Process has to be running in order to be terminated.");
		return LV_RESULT_INVALID_STATE;
	}

#if (LV_WINDOWS)
	if (!TerminateProcess(process->__handle, exit_code)) {
		return LV_RESULT_SUBROUTINE_FAILED;
	}

	return 0;
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif

	return LV_RESULT_UNEXP_TERMINATION;
}
