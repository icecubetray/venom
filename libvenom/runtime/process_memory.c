/*******************************************************************************
**                                                                            **
**   The MIT License                                                          **
**                                                                            **
**   Copyright 2017 icecubetray                                               **
**                                                                            **
**   Permission is hereby granted, free of charge, to any person              **
**   obtaining a copy of this software and associated documentation files     **
**   (the "Software"), to deal in the Software without restriction,           **
**   including without limitation the rights to use, copy, modify, merge,     **
**   publish, distribute, sublicense, and/or sell copies of the Software,     **
**   and to permit persons to whom the Software is furnished to do so,        **
**   subject to the following conditions:                                     **
**                                                                            **
**   The above copyright notice and this permission notice shall be           **
**   included in all copies or substantial portions of the Software.          **
**                                                                            **
**   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          **
**   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       **
**   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   **
**   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY     **
**   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,     **
**   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        **
**   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                   **
**                                                                            **
********************************************************************************
**
**  Notes:
**    -
**
*/

#include "./process_memory.h"




#define PROCESS_MUST_BE_RUNNING				LV_STR_STATE_ERROR LV_STR_SEPARATOR "process must be running in order to access its memory"




lv_result_t lv_process_memr(const lv_process_t *const restrict process, const void *address, void *const restrict buffer, const size_t nsize, size_t *const restrict nread) {
	if (!process || !address || !buffer) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!nsize) {
		return LV_RESULT_SUCCESS;
	}

	if (!LV_IS_PROCESS_RUNNING(process)) {
		log(PROCESS_MUST_BE_RUNNING);
		return LV_RESULT_INVALID_STATE;
	}

#if (LV_WINDOWS)
	if (!ReadProcessMemory(process->__handle, address, buffer, nsize, nread)) {
		return 1;
	}

	return LV_RESULT_SUCCESS;
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif
}




lv_result_t lv_process_memw(const lv_process_t *const restrict process, void *const restrict address, const void *const restrict buffer, const size_t nsize, size_t *const restrict nwritten) {
	if (!process || !address || !buffer) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!nsize) {
		return LV_RESULT_SUCCESS;
	}

	if (!LV_IS_PROCESS_RUNNING(process)) {
		log(PROCESS_MUST_BE_RUNNING);
		return LV_RESULT_INVALID_STATE;
	}

#if (LV_WINDOWS)
	if (!WriteProcessMemory(process->__handle, address, buffer, nsize, nwritten)) {
		return 1;
	}

	return LV_RESULT_SUCCESS;
#else
	__UNSUPPORTED(LV_RESULT_UNSUPPORTED)
#endif
}




lv_result_t lv_process_memr16(const lv_process_t *const restrict process, const void *const restrict address, uint16_t *const restrict value) {
	size_t read = 0;
	lv_result_t result = lv_process_memr(process, address, value, sizeof(*value), &read);
	if (result != 0) {
		return result;
	}

	if (read != sizeof(*value)) {
		return 2;
	}

	return LV_RESULT_SUCCESS;
}




lv_result_t lv_process_memw16(const lv_process_t *const restrict process, void *const restrict address, const uint16_t value) {
	size_t written = 0;
	lv_result_t result = lv_process_memw(process, address, &value, sizeof(value), &written);
	if (result != 0) {
		return result;
	}

	if (written != sizeof(value)) {
		return 2;
	}

	return LV_RESULT_SUCCESS;
}




lv_result_t lv_process_memr32(const lv_process_t *const restrict process, const void *const restrict address, uint32_t *const restrict value) {
	size_t read = 0;
	lv_result_t result = lv_process_memr(process, address, value, sizeof(*value), &read);
	if (result != 0) {
		return result;
	}

	if (read != sizeof(*value)) {
		return 2;
	}

	return LV_RESULT_SUCCESS;
}




lv_result_t lv_process_memw32(const lv_process_t *const restrict process, void *const restrict address, const uint32_t value) {
	size_t written = 0;
	lv_result_t result = lv_process_memw(process, address, &value, sizeof(value), &written);
	if (result != 0) {
		return result;
	}

	if (written != sizeof(value)) {
		return 2;
	}

	return LV_RESULT_SUCCESS;
}




lv_result_t lv_process_memr64(const lv_process_t *const restrict process, const void *const restrict address, uint64_t *const restrict value) {
	size_t read = 0;
	lv_result_t result = lv_process_memr(process, address, value, sizeof(*value), &read);
	if (result != 0) {
		return result;
	}

	if (read != sizeof(*value)) {
		return 2;
	}

	return LV_RESULT_SUCCESS;
}




lv_result_t lv_process_memw64(const lv_process_t *const restrict process, void *const restrict address, const uint64_t value) {
	size_t written = 0;
	lv_result_t result = lv_process_memw(process, address, &value, sizeof(value), &written);
	if (result != 0) {
		return result;
	}

	if (written != sizeof(value)) {
		return 2;
	}

	return LV_RESULT_SUCCESS;
}




lv_result_t lv_process_memderef_ex(const lv_process_t *const restrict process, const void *const *const restrict address, const size_t offset, const void **const restrict value, const uint_fast8_t ptr_size) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!LV_IS_PROCESS_RUNNING(process)) {
		log(PROCESS_MUST_BE_RUNNING);
		return LV_RESULT_INVALID_STATE;
	}

	if (ptr_size != 4 && ptr_size != 8) {
		log("Invalid pointer size specified.");
		return LV_RESULT_CHECK_FAIL;
	}

	if (ptr_size > sizeof(void*)) {
		log("Target process pointer size greater than own pointer size.");
		return LV_RESULT_CHECK_FAIL;
	}

	uint32_t v32 = 0;
	uint64_t v64 = 0;

	size_t read = 0;
	lv_result_t result = lv_process_memr(process, address, ((ptr_size == 4) ? (void*)&v32 : (void*)&v64), ptr_size, &read);
	if (result != 0) {
		return result;
	}

	if (read != ptr_size) {
		return 2;
	}

	*value = (void*)(uintptr_t)(((ptr_size == 4) ? v32 : v64) + offset);
	return LV_RESULT_SUCCESS;
}

lv_result_t lv_process_memderef(const lv_process_t *const restrict process, const void *const *const restrict address, const size_t offset, const void **const restrict value) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	return lv_process_memderef_ex(process, address, offset, value, (LV_FLAG(process->flags, LV_PROCESS_32BIT) ? 4 : (LV_FLAG(process->flags, LV_PROCESS_64BIT) ? 8 : 0)));
}




lv_result_t lv_process_memderefmul_ex(const lv_process_t *const restrict process, const void *const *const restrict address, const size_t *const restrict offsets, const uint32_t noffsets, const void **const restrict value, const uint_fast8_t ptr_size) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!LV_IS_PROCESS_RUNNING(process)) {
		log(PROCESS_MUST_BE_RUNNING);
		return LV_RESULT_INVALID_STATE;
	}

	if (!offsets) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	if (!noffsets) {
		return LV_RESULT_PARAMETER_INVALID;
	}

	if (ptr_size != 4 && ptr_size != 8) {
		log("Invalid pointer size specified.");
		return LV_RESULT_CHECK_FAIL;
	}

	if (ptr_size > sizeof(void*)) {
		log("Target process pointer size greater than own pointer size.");
		return LV_RESULT_CHECK_FAIL;
	}

	uint32_t v32 = 0;
	uint64_t v64 = 0;
	if (ptr_size == 4) {
		v32 = (uint32_t)(uintptr_t)address;
	} else {
		v64 = (uint64_t)(uintptr_t)address; // (ULONG) cast added for C4826
	}

	lv_result_t result;
	unsigned int i;
	for (i = 0; i < noffsets; ++i) {
		result = lv_process_memderef_ex(process, (void*)(uintptr_t)((ptr_size == 4) ? v32 : v64), offsets[i], ((ptr_size == 4) ? (void*)&v32 : (void*)&v64), ptr_size);
		if (result != 0) {
			return (LV_RESULT_FLAG_SUBROUTINE | result);
		}
	}

	*value = (void*)(uintptr_t)((ptr_size == 4) ? v32 : v64);
	return 0;
}

lv_result_t lv_process_memderefmul(const lv_process_t *const restrict process, const void *const *const restrict address, const size_t *const restrict offsets, const uint32_t noffsets, const void **const restrict value) {
	if (!process) {
		log(LV_STR_NULLPOINTER);
		return LV_RESULT_NULLPOINTER;
	}

	return lv_process_memderefmul_ex(process, address, offsets, noffsets, value, (LV_FLAG(process->flags, LV_PROCESS_32BIT) ? 4 : (LV_FLAG(process->flags, LV_PROCESS_64BIT) ? 8 : 0)));
}
