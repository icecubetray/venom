/*******************************************************************************
**                                                                            **
**   The MIT License                                                          **
**                                                                            **
**   Copyright 2017 icecubetray                                               **
**                                                                            **
**   Permission is hereby granted, free of charge, to any person              **
**   obtaining a copy of this software and associated documentation files     **
**   (the "Software"), to deal in the Software without restriction,           **
**   including without limitation the rights to use, copy, modify, merge,     **
**   publish, distribute, sublicense, and/or sell copies of the Software,     **
**   and to permit persons to whom the Software is furnished to do so,        **
**   subject to the following conditions:                                     **
**                                                                            **
**   The above copyright notice and this permission notice shall be           **
**   included in all copies or substantial portions of the Software.          **
**                                                                            **
**   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          **
**   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       **
**   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   **
**   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY     **
**   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,     **
**   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        **
**   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                   **
**                                                                            **
********************************************************************************
**
**  Notes:
**    -
**
*/

#ifndef __LV_CORE_PLATFORM_H
#define __LV_CORE_PLATFORM_H




// Enumerate identifiers.
#define LV_X86_ID							1
#define LV_X64_ID							2
#define LV_ARM_ID							3

// Try to determine architecture using predefs if one wasn't specified.
#ifndef LV_ARCH_ID
#	if (defined(__amd64) || defined(__amd64__) || defined(__x86_64) || defined(__x86_64__) || defined(__ppc64__) || defined(_M_X64) || defined(_M_AMD64))
#		define LV_ARCH_ID					LV_X64_ID
#	elif (defined(__arm) || defined(__arm__) || defined(__thumb__) || defined(_ARM) || defined(_M_ARM) || defined(_M_ARMT) || defined(__ARMCC_VERSION) || defined(__CC_ARM))
#		define LV_ARCH_ID					LV_ARM_ID
#	else
#		define LV_ARCH_ID					LV_X86_ID
#	endif
#endif

// Define preprocessor shortcuts.
#if (LV_ARCH_ID == LV_X86_ID)
#	define LV_X86							1
#elif (LV_ARCH_ID == LV_X64_ID)
#	define LV_X64							1
#elif (LV_ARCH_ID == LV_ARM_ID)
#	define LV_ARM							1
#else
#	error Unknown architecture.
#endif




// Enumerate system identifiers.
#define LV_LINUX_ID							1
#define LV_WINDOWS_ID						2
#define LV_MAC_ID							3

// Try to determine operating system using predefs if one wasn't specified.
#ifndef LV_OS_ID
#	if (defined(__gnu_linux__) || defined(__linux__) || defined(linux))
#		define LV_OS_ID						LV_LINUX_ID
#	elif (defined(_WIN32))
#		define LV_OS_ID						LV_WINDOWS_ID
#	elif (defined(__APPLE__) && defined(__MACH__))
#		define LV_OS_ID						LV_MAC_ID
#	endif
#endif

// Define preprocessor shortcuts.
#if (LV_OS_ID == LV_LINUX_ID)
#	define LV_LINUX							1

#	include <errno.h>
#elif (LV_OS_ID == LV_WINDOWS_ID)
#	define LV_WINDOWS						1

#	define WIN32_LEAN_AND_MEAN
#	define NOGDICAPMASKS
#	define NOWINMESSAGES
#	define NOWINSTYLES
#	define NOSYSMETRICS
#	define NOMENUS
#	define NOICONS
#	define NOKEYSTATES
#	define NOSYSCOMMANDS
#	define NORASTEROPS
#	define NOSHOWWINDOW
#	define NOATOM
#	define NOCLIPBOARD
#	define NOCOLOR
#	define NOCTLMGR
#	define NODRAWTEXT
#	define NOGDI
#	define NOMB
#	define NOMEMMGR
#	define NOMETAFILE
#	define NOMINMAX
#	define NOMSG
#	define NOOPENFILE
#	define NOSCROLL
#	define NOSERVICE
#	define NOSOUND
#	define NOTEXTMETRIC
#	define NOWH
#	define NOWINOFFSETS
#	define NOCOMM
#	define NOKANJI
#	define NOHELP
#	define NOPROFILER
#	define NODEFERWINDOWPOS
#	define NOMCX

#	if (LV_EXPORTING)
#		define _WIN32_WINNT						0x0501
#	endif

#	include <Windows.h>
#elif (LV_OS_ID == LV_MAC_ID)
#	define LV_MAC							1
#else
#	error Unknown operating system.
#endif




// Enumerate identifiers.
#define LV_LITTLE_ID						1	// Little-endian byte ordering.
#define LV_BIG_ID							2	// Big-endian byte ordering.

// Try to determine endianness using predefs if one wasn't specified.
#ifndef LV_ENDIANNESS
#	if (defined(__BIG_ENDIAN__) || defined(__ARMEB__) || defined(__THUMBEB__) || defined(__AARCH64EB__) || defined(_MIPSEB) || defined(__MIPSEB) || defined(__MIPSEB__) || ((LV_WINDOWS && REG_DWORD != REG_DWORD_LITTLE_ENDIAN) || (!LV_WINDOWS && ((__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)))))
#		define LV_ENDIANNESS				LV_BIG_ID // Big-endian byte ordering was detected.
#	else
#		define LV_ENDIANNESS				LV_LITTLE_ID // Little-endian byte ordering was detected.
#	endif
#endif

// Define preprocessor shortcuts.
#if (LV_ENDIANNESS == LV_BIG_ID)
#	define LV_BIG							1
#elif (LV_ENDIANNESS == LV_LITTLE_ID)
#	define LV_LITTLE						1
#else
#	error Unknown endianness.
#endif




// Enumerate identifiers.
#define LV_GCC_ID							1
#define LV_MSC_ID							2
#define LV_LLVM_ID							3

// Try to determine compiler using predefs if one wasn't specified.
#ifndef LV_COMPILER_ID
#	if (defined(_MSC_VER))
#		define LV_COMPILER_ID				LV_MSC_ID
#	elif (defined(__llvm__) || defined(__clang__))
#		define LV_COMPILER_ID				LV_LLVM_ID
#	elif (defined(__GNUC__))
#		define LV_COMPILER_ID				LV_GCC_ID
#	endif
#endif

// Define preprocessor shortcuts and compiler-specific information.
#if ((LV_COMPILER_ID == LV_GCC_ID) || (LV_COMPILER_ID == LV_LLVM_ID))
#	if (LV_COMPILER_ID == LV_GCC_ID)
#		define LV_GCC						1
#	else
#		define LV_LLVM						1
#	endif
#
#	include "./macrodefs.h"
#
#	define __LV_ATTR(x)						__attribute__((x))
#
#	define LV_COMPILER_LOG(mesg)			_Pragma(LV_MSTR(message mesg))
#	define LV_EXPORT						__attribute((visibility("default")))
#	define LV_IMPORT
#	define LV_INTERNAL						__attribute((visibility("hidden")))
#
#	define __non_null(params)				__LV_ATTR(nonnull params)
#	define non_null(...)					__non_null((__VA_ARGS__))
#
#	include <x86intrin.h>
#	define LV_INTRINSICS					1
#elif (LV_COMPILER_ID == LV_MSC_ID)
#	define LV_MSC							1
#
#	define __LV_ATTR(x)						__declspec(x)
#
#	define LV_COMPILER_LOG(mesg)			__pragma(message(mesg))
#	define LV_EXPORT						__declspec(dllexport)
#	define LV_IMPORT						__declspec(dllimport)
#	define LV_INTERNAL
#
#	define restrict							__restrict
#	define __non_null(params)
#	define non_null(...)
#
#	include <intrin.h>
#	define LV_INTRINSICS					1
#endif

#ifndef LV_INTRINSICS
#	define LV_INTRINSICS					0
#endif




#endif
