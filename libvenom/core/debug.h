/*******************************************************************************
**                                                                            **
**   The MIT License                                                          **
**                                                                            **
**   Copyright 2017 icecubetray                                               **
**                                                                            **
**   Permission is hereby granted, free of charge, to any person              **
**   obtaining a copy of this software and associated documentation files     **
**   (the "Software"), to deal in the Software without restriction,           **
**   including without limitation the rights to use, copy, modify, merge,     **
**   publish, distribute, sublicense, and/or sell copies of the Software,     **
**   and to permit persons to whom the Software is furnished to do so,        **
**   subject to the following conditions:                                     **
**                                                                            **
**   The above copyright notice and this permission notice shall be           **
**   included in all copies or substantial portions of the Software.          **
**                                                                            **
**   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          **
**   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       **
**   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   **
**   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY     **
**   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,     **
**   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        **
**   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                   **
**                                                                            **
********************************************************************************
**
**  Notes:
**    -
**
*/

#ifndef __LV_CORE_DEBUG_H
#define __LV_CORE_DEBUG_H




#define LV_STR_SEPARATOR					": "
#define LV_STR_NULLPOINTER					"Unexpected null-pointer encountered."
#define LV_STR_PARAM_ERROR					"Parameter error."
#define LV_STR_STATE_ERROR					"State error."




#if (LV_DEBUG)
#	include <stdio.h>
#	define log(mesg)						puts(mesg)
#	define logf(format, ...)				printf(format"\n", __VA_ARGS__)
#else
#	define log(disabled)
#	define logf(disabled, ...)
#endif




#include "./macrodefs.h"

#ifdef LV_COMPILER_LOG
#	define __UNSUPPORTED_LOG				LV_COMPILER_LOG("!! Implementation missing due to lack of support at line " LV_MSTR(__LINE__) " of " __FILE__ ".");
#	define __UNSUPPORTED(ret)				__UNSUPPORTED_LOG; \
											return (ret);
#	define __UNSUPPORTED_VOID()				__UNSUPPORTED_LOG; \
											return;
#else
#	define __UNSUPPORTED(ret)				return (ret);
#	define __UNSUPPORTED_VOID()				return;
#endif




#endif
