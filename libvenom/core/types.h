/*******************************************************************************
**                                                                            **
**   The MIT License                                                          **
**                                                                            **
**   Copyright 2017 icecubetray                                               **
**                                                                            **
**   Permission is hereby granted, free of charge, to any person              **
**   obtaining a copy of this software and associated documentation files     **
**   (the "Software"), to deal in the Software without restriction,           **
**   including without limitation the rights to use, copy, modify, merge,     **
**   publish, distribute, sublicense, and/or sell copies of the Software,     **
**   and to permit persons to whom the Software is furnished to do so,        **
**   subject to the following conditions:                                     **
**                                                                            **
**   The above copyright notice and this permission notice shall be           **
**   included in all copies or substantial portions of the Software.          **
**                                                                            **
**   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          **
**   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       **
**   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   **
**   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY     **
**   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,     **
**   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        **
**   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                   **
**                                                                            **
********************************************************************************
**
**  Notes:
**    -
**
*/

#ifndef __LV_CORE_TYPES_H
#define __LV_CORE_TYPES_H




#include "./platform.h"
#include <stdint.h>


#if (LV_WINDOWS)
typedef DWORD lv_pid_t;
#elif (LV_LINUX)
#	include <sys/types.h>

typedef pid_t lv_pid_t;
#endif

#if (LV_MSC)
#	define size_t							SIZE_T	// Fix compiler complaining about indirection (C4057).
#endif


typedef int32_t lv_result_t;

typedef uint16_t lv_flags_t;




#define LV_RESULT_SUCCESS					0

#define LV_RESULT_NULLPOINTER				(lv_result_t)0x80000001
#define LV_RESULT_PARAMETER_INVALID			(lv_result_t)0x80000002
#define LV_RESULT_CHECK_FAIL				(lv_result_t)0x80000003
#define LV_RESULT_INVALID_STATE				(lv_result_t)0x80000004
#define LV_RESULT_SUBROUTINE_FAILED			(lv_result_t)0x80000005
#define LV_RESULT_INVALID_FLAGS				(lv_result_t)0x80000006
#define LV_RESULT_TIMEOUT					(lv_result_t)0x80000007
#define LV_RESULT_INVALID_DATA				(lv_result_t)0x80000008

#define LV_RESULT_UNSUPPORTED				(lv_result_t)0xA0000001
#define LV_RESULT_UNEXP_TERMINATION			(lv_result_t)0xA0000002
#define LV_RESULT_INIT_FAILED				(lv_result_t)0xA0000003

#define LV_RESULT_FLAG_SUBROUTINE			(lv_result_t)0x40000000




#endif
