/*******************************************************************************
**                                                                            **
**   The MIT License                                                          **
**                                                                            **
**   Copyright 2017 icecubetray                                               **
**                                                                            **
**   Permission is hereby granted, free of charge, to any person              **
**   obtaining a copy of this software and associated documentation files     **
**   (the "Software"), to deal in the Software without restriction,           **
**   including without limitation the rights to use, copy, modify, merge,     **
**   publish, distribute, sublicense, and/or sell copies of the Software,     **
**   and to permit persons to whom the Software is furnished to do so,        **
**   subject to the following conditions:                                     **
**                                                                            **
**   The above copyright notice and this permission notice shall be           **
**   included in all copies or substantial portions of the Software.          **
**                                                                            **
**   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,          **
**   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       **
**   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.   **
**   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY     **
**   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,     **
**   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        **
**   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                   **
**                                                                            **
********************************************************************************
**
**  Notes:
**    -
**
*/

#include "./time.h"

#if (LV_MSC)
#	pragma warning(push)
#	pragma warning(disable: 4820)
#endif

#include <time.h>

#if (LV_MSC)
#	pragma warning(pop)
#endif


uint64_t lv_rdtsc() {
#if (LV_MSC)
#	if (LV_INTRINSICS)
	return __rdtsc();
#	else
	/*uint32_t hi, lo;
	__asm {
		rdtsc
		mov hi, edx
		mov lo, eax
	};
	return ((((uint64_t)hi) << 32) | lo);*/
	uint64_t tsc = 0;
	__asm {
		/*push edx
		push eax
		push ebx*/

		rdtsc

		lea ebx, tsc

#		if (LV_BIG)
		mov dword [ebx + 0], edx
		mov dword [ebx + 4], eax
#		else
		mov dword ptr [ebx + 4], edx
		mov dword ptr [ebx + 0], eax
#		endif

		/*pop edx
		pop eax
		pop ebx*/
	};
	return tsc;
#	endif
#elif (LV_LINUX)
#	if (LV_INTRINSICS)
	return __rdtsc();
#	else
	uint64_t tsc = 0;

	asm volatile (
		"rdtsc\n\t"
		"shl $32, %%rdx\n\t"
		"or %%rdx, %0"
		: "=a" (tsc)
		:
		: "rdx"
	);

	return tsc;
#	endif
#else
	__UNSUPPORTED(0)
#endif
	return 0;
}


uint64_t lv_nanos() {
#if (LV_WINDOWS)
	FILETIME ft;
	GetSystemTimeAsFileTime(&ft);
	return (((((uint64_t)ft.dwHighDateTime << 32) | ft.dwLowDateTime) - 0x295E96488640000U) * 100);
#else
	struct timespec ts;
	if (clock_gettime(CLOCK_REALTIME, &ts) == 0) {
		return (ts.tv_sec * 1000000000) + ts.tv_nsec;
	}
	return 0;
#endif
}


void lv_sleep_nanos(const uint64_t nanos) {
#if (LV_WINDOWS)
	const DWORD s = (DWORD)(nanos / 1000000);
	Sleep((s ? s : 1));
#else
	struct timespec ts = {
		.tv_sec = (nanos / 1000000000),
		.tv_nsec = (nanos - (ts.tv_sec * 1000000000))
	};

	while (clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &ts, &ts) == EINTR) {
		;
	}
#endif
}
