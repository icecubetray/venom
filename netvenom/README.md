﻿# NetVenom
C# binding for [libvenom](../libvenom).

## Note
Within [netvenom.csproj](./netvenom.csproj) there is a conditional compilation option at the top, namely IncludeNativeInResources.
If you want to include the native libraries in the resources of the binding (so you don't need to ship them explicitly), set it to 1.
If you don't, set it to something else.
