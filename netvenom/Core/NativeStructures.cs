﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NetVenom.Core
{
	public static class NativeStructures
	{
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public delegate void ProcessExitCallback(IntPtr instance);

		[StructLayout(LayoutKind.Sequential)]
		public struct lv_process_t
		{
			#region Static
			public enum Member
			{
				exit_callback,
#if (LV_WINDOWS)
				handle,
				wait_handle,
#endif
				identifier,
				exit_code,
				flags,
				magic
			}

			internal static readonly int Size = Marshal.SizeOf(typeof(lv_process_t));

			public static int OffsetOf(Member member) {
				if (!Enum.IsDefined(typeof(Member), member)) {
					return -1;
				}
				
				return (int)Marshal.OffsetOf(typeof(lv_process_t), member.ToString());
			}

			internal static IntPtr LocationOf(IntPtr instance, Member member) {
				if (instance == IntPtr.Zero) {
					return IntPtr.Zero;
				}

				int offset = OffsetOf(member);
				if (offset < 0) {
					return IntPtr.Zero;
				}

				return (instance + offset);
			}
			#endregion

			[MarshalAs(UnmanagedType.FunctionPtr)]
			ProcessExitCallback exit_callback;

#if (LV_WINDOWS)
			private IntPtr handle;
			private IntPtr wait_handle;
#endif

			[MarshalAs(UnmanagedType.U4)]
			internal uint identifier;

			[MarshalAs(UnmanagedType.U4)]
			internal uint exit_code;

			[MarshalAs(UnmanagedType.U2)]
			internal ushort flags;

			[MarshalAs(UnmanagedType.U2)]
			internal ushort magic;
		}
	}
}
