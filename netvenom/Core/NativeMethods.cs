﻿using NetVenom.Formats;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static NetVenom.Core.NativeStructures;

namespace NetVenom.Core
{
	internal static class NativeMethods
	{
#if (LV_WINDOWS)
		internal const string LibExtension = ".dll";
#elif (LV_LINUX)
		internal const string LibExtension = ".so";
#else
#error Unsupported platform.
#endif
		internal const string LibName = "libvenom" + LibExtension;
		internal const CharSet Cs = CharSet.Ansi;
		internal const CallingConvention Cc = CallingConvention.Cdecl;
		internal static readonly string LibPath = null;
		internal static readonly string LocalLibPath = Path.GetFullPath(Path.GetDirectoryName(Application.ExecutablePath)) + "/" + LibName;

#if (LV_NATIVE_IN_RESOURCES)
		/// <summary>
		/// Gets the bytes of the appropriate native library to use for P/Invoke that was shipped with this assembly.
		/// </summary>
		private static byte[] GetNativeLibrary() {
			if (VenomPE.CurrentMachine == VenomPE.Machine.x64) {
				return Properties.Resources.libvenom_x64;
			} else if (VenomPE.CurrentMachine == VenomPE.Machine.x86) {
				return Properties.Resources.libvenom_x86;
			} else {
				throw new BadImageFormatException("Unsupported architecture.");
			}
		}
#endif

		static string GetVenomInstallationPath() {
			string path;

			path = LocalLibPath;
			if (File.Exists(path)) {
				return path;
			}

			// TODO: check GAC etc.

			return null;
		}

		static NativeMethods() {
__retry:
			try {
				LibPath = GetVenomInstallationPath();

#if (!LV_NATIVE_IN_RESOURCES)
				if (LibPath == null) {
					switch (MessageBox.Show("The venom library (" + LibName + ") was not found on this system. This application may not be able to run without it.\r\n\r\nPress Abort if you're sick of it or just want to quit.\r\nPress Retry if you just installed a copy and want to check again.\r\nPress Ignore if you feel lucky.", "NetVenom P/Invoke Guard", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error)) {
						case DialogResult.Retry:
							goto __retry;
						case DialogResult.Ignore:
							goto __ignore;
						default:
							Environment.Exit(1);
							break;
					}
				}
#else
				byte[] assembly_bin = GetNativeLibrary();

				VenomBasicPE pe = new VenomBasicPE(assembly_bin), pe_installed = null;
				if (!pe.signature_file_valid || !pe.signature_header_valid) {
					throw new BadImageFormatException("The shipped library contains invalid signatures.");
				}
				if (pe.machine != VenomPE.CurrentMachine) {
					throw new BadImageFormatException("The shipped library seems to be compiled for a different architecture.");
				}
				
				if (LibPath == null) {
					Log.Debug("Library doesn't seem to be installed anywhere, writing to executable directory.");
					File.WriteAllBytes(LocalLibPath, assembly_bin);
					return;
				}

				bool rewrite = false;

				if (Environment.GetCommandLineArgs().Contains("--lv-force-rewrite", StringComparer.InvariantCultureIgnoreCase)) {
					Log.Debug("Commandline switch --lv-force-rewrite was specified.");
					rewrite = true;
					goto __rewrite;
				}

				try {
					using (FileStream stream = new FileStream(LibPath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read)) {
						pe_installed = new VenomBasicPE(stream);
					}
				} catch { }

				if (pe_installed == null) {
					Log.Debug("Failed to read PE header of installed library.");
					rewrite = true;
					goto __rewrite;
				}

				if (!pe_installed.signature_file_valid || !pe_installed.signature_header_valid) {
					Log.Debug("The installed library has invalid PE signatures.");
					rewrite = true;
					goto __rewrite;
				}

				if (pe_installed.machine != VenomPE.CurrentMachine) {
					Log.Debug("The installed library is compiled for a different architecture.");
					rewrite = true;
					goto __rewrite;
				}

#if (LV_NATIVE_AUTO_UPDATE)
				if (pe.timestamp > pe_installed.timestamp) {
					Log.Debug("Auto-update: the installed library is older than the shipped library.");
					rewrite = true;
					goto __rewrite;
				}
#endif

__rewrite:
				if (rewrite) {
					Log.Debug("Rewrite.");
					File.WriteAllBytes("./libvenom.dll", assembly_bin);
				}

				assembly_bin = null;
#endif
			} catch (Exception ex) {
				MessageBox.Show("Unknown exception in the NativeMethods constructor.\r\n\r\nException message:\r\n" + (ex?.Message ?? "Unspecified."), "NetVenom", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Environment.Exit(1);
			}
__ignore:;
		}

		#region process.h
		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_open_ex(
			[In] IntPtr process,
			[In] ushort flags,
			[In] uint identifier,
			[In] string wndText,
			[In] [MarshalAs(UnmanagedType.FunctionPtr)] ProcessExitCallback exit_callback
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_open(
			[In] IntPtr process,
			[In] ushort flags,
			[In] uint identifier,
			[In] string wndText
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_open_await_window(
			[In] IntPtr process,
			[In] ushort flags,
			[In] string wndText,
			[In] ulong ns_interval,
			[In] ulong ns_timeout
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_close(
			[In] IntPtr process
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_register_exit_callback(
			[In] IntPtr process,
			[In] [MarshalAs(UnmanagedType.FunctionPtr)] ProcessExitCallback exit_callback
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_unregister_exit_callback(
			[In] IntPtr process
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_suspend(
			[In] IntPtr process
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_resume(
			[In] IntPtr process
		);

		/*[DllImport(DllName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_get_suspended(
			[In] IntPtr process
		);*/

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_terminate(
			[In] IntPtr process,
			[In] uint exit_code
		);
		#endregion

		#region process_memory.h
		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memr(
			[In] IntPtr process,
			[In] IntPtr address,
			[Out] byte[] buffer,
			[In] IntPtr nbytes,
			out IntPtr nread
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memw(
			[In] IntPtr process,
			[In] IntPtr address,
			[In] byte[] buffer,
			[In] IntPtr nbytes,
			out IntPtr nwritten
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memr16(
			[In] IntPtr process,
			[In] IntPtr address,
			out ushort value
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memw16(
			[In] IntPtr process,
			[In] IntPtr address,
			[In] ushort value
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memr32(
			[In] IntPtr process,
			[In] IntPtr address,
			out uint value
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memw32(
			[In] IntPtr process,
			[In] IntPtr address,
			[In] uint value
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memr64(
			[In] IntPtr process,
			[In] IntPtr address,
			out ulong value
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memw64(
			[In] IntPtr process,
			[In] IntPtr address,
			[In] ulong value
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memderef(
			[In] IntPtr process,
			[In] IntPtr address,
			[In] IntPtr offset,
			out IntPtr value
		);

		[DllImport(LibName, CallingConvention = Cc, CharSet = Cs)]
		internal extern static int lv_process_memderefmul(
			[In] IntPtr process,
			[In] IntPtr address,
			[In] IntPtr[] offsets,
			[In] uint noffsets,
			out IntPtr value
		);
		#endregion
	}
}
