﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetVenom.Core
{
	internal static class Memory
	{
		private static void CheckParams(byte[] buffer, uint index, int sizereq) {
			if (buffer == null) {
				throw new ArgumentNullException();
			}

			if ((index < 0) || ((index + sizereq) > buffer.Length)) {
				throw new ArgumentOutOfRangeException();
			}
		}

		internal static ushort GetUInt16LE(byte[] buffer, uint index) {
			CheckParams(buffer, index, 2);
			return (ushort)(buffer[index + 0] | (buffer[index + 1] << 8));
		}
		internal static ushort GetUInt16BE(byte[] buffer, uint index) {
			CheckParams(buffer, index, 2);
			return (ushort)((buffer[index + 0] << 8) | buffer[index + 1]);
		}

		internal static uint GetUInt32LE(byte[] buffer, uint index) {
			CheckParams(buffer, index, 4);
			return (uint)(buffer[index + 0] | (buffer[index + 1] << 8) | (buffer[index + 2] << 16) | (buffer[index + 3] << 24));
		}
		internal static uint GetUInt32BE(byte[] buffer, uint index) {
			CheckParams(buffer, index, 4);
			return (uint)((buffer[index + 0] << 24) | (buffer[index + 1] << 16) | (buffer[index + 2] << 8) | buffer[index + 3]);
		}
	}
}
