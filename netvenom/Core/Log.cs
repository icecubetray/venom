﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetVenom.Core
{
	internal static class Log
	{
		internal static void Debug(string str) {
			if (System.Diagnostics.Debugger.IsAttached) {
				System.Diagnostics.Debug.WriteLine(str);
			} else {
				Console.WriteLine(str);
			}
		}
	}
}
