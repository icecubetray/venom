﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetVenom.Runtime
{
	partial class VenomProcess
	{
		public static VenomProcess ByWindow(ProcessFlags flags, string windowText) {
			try {
				return new VenomProcess(flags, windowText);
			} catch {
				return null;
			}
		}
		
		public static VenomProcess AwaitWindowNanos(ProcessFlags flags, string windowText, ulong ns_interval, ulong ns_timeout) {
			try {
				return new VenomProcess(flags, windowText, ns_interval, ns_timeout);
			} catch {
				return null;
			}
		}

		public static VenomProcess AwaitWindowMillis(ProcessFlags flags, string windowText, ulong ms_interval, ulong ms_timeout) {
			try {
				return new VenomProcess(flags, windowText, (ms_interval * 1000000), (ms_timeout * 1000000));
			} catch {
				return null;
			}
		}

		public static VenomProcess AwaitWindowSecs(ProcessFlags flags, string windowText, ulong s_interval, ulong s_timeout) {
			try {
				return new VenomProcess(flags, windowText, (s_interval * 1000000000), (s_timeout * 1000000000));
			} catch {
				return null;
			}
		}
	}
}
