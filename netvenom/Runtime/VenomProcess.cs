﻿using NetVenom.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using static NetVenom.Core.NativeMethods;
using static NetVenom.Core.NativeStructures;


namespace NetVenom.Runtime
{
	public enum ProcessFlags : ushort
	{
		Running = 0x0100,
		Platform32Bits = 0x0200,
		Platform64Bits = 0x0400,
		RegisteredExit = 0x0800,
		Suspended = 0x1000,
		Terminated = 0x2000,

		MemoryRead = 0x0001,
		MemoryWrite = 0x0002,
		MemoryReadWrite = (MemoryRead | MemoryWrite),
		RegisterExit = 0x0004,
		Suspendable = 0x0008,
		Terminatable = 0x0010,

		ErrorExitCode = 0x8000
	}

	public enum ProcessImageType
	{
		Undetermined = 0,
		Platform32Bits = 1,
		Platform64Bits = 2
	}

	public sealed partial class VenomProcess : IDisposable
	{
		#region Private Fields
		private IntPtr instance;
		#endregion

		#region Events
		public delegate void VenomProcessExitedDelegate(VenomProcess process, uint exit_code);
		public event VenomProcessExitedDelegate Exited;
		#endregion

		#region Properties
		public uint Identifier {
			get {
				IntPtr ptr = lv_process_t.LocationOf(this.instance, lv_process_t.Member.identifier);
				if (ptr == IntPtr.Zero) {
					throw new Exception("Unable to get the memory location of the identifier.");
				}

				return (uint)Marshal.ReadInt32(ptr);
			}
		}

		public ProcessImageType ImageType {
			get {
				IntPtr ptr = lv_process_t.LocationOf(this.instance, lv_process_t.Member.flags);
				if (ptr == IntPtr.Zero) {
					throw new Exception("Unable to get the memory location of the flags.");
				}

				ushort flags = (ushort)Marshal.ReadInt16(ptr);
				if ((flags & (ushort)ProcessFlags.Platform64Bits) == (ushort)ProcessFlags.Platform64Bits) {
					return ProcessImageType.Platform64Bits;
				} else if ((flags & (ushort)ProcessFlags.Platform32Bits) == (ushort)ProcessFlags.Platform32Bits) {
					return ProcessImageType.Platform32Bits;
				} else {
					return ProcessImageType.Undetermined;
				}
			}
		}
		#endregion

		#region Constructors
		private VenomProcess() : base() {
		}

		public VenomProcess(ProcessFlags flags, uint identifier) : this() {
			if (identifier == 0) {
				throw new ArgumentException("Identifier may not be zero.");
			}

			this.instance = Marshal.AllocHGlobal(lv_process_t.Size);
			int result = lv_process_open_ex(this.instance, (ushort)(flags | ProcessFlags.RegisterExit), identifier, null, ExitCallback);
			if (result != 0) {
				Marshal.FreeHGlobal(this.instance);
				throw new Exception("Failed to open process with identifier " + identifier.ToString() + ": " + result.ToString());
			}
		}

		public VenomProcess(ProcessFlags flags, string windowText) : this() {
			if (windowText == null) {
				throw new ArgumentNullException("Window text may not be null.");
			}

			this.instance = Marshal.AllocHGlobal(lv_process_t.Size);
			int result = lv_process_open_ex(this.instance, (ushort)(flags | ProcessFlags.RegisterExit), 0, windowText, ExitCallback);
			if (result != 0) {
				Marshal.FreeHGlobal(this.instance);
				throw new Exception("Failed to open process with window text \"" + windowText + "\": " + result.ToString());
			}
		}

		public VenomProcess(ProcessFlags flags, string windowText, ulong ns_interval, ulong ns_timeout) : this() {
			if (windowText == null) {
				throw new ArgumentNullException("Window text may not be null.");
			}

			this.instance = Marshal.AllocHGlobal(lv_process_t.Size);
			int result = lv_process_open_await_window(this.instance, (ushort)(flags | ProcessFlags.RegisterExit), windowText, ns_interval, ns_timeout);
			if (result != 0) {
				Marshal.FreeHGlobal(this.instance);
				throw new Exception("Failed to open process with window text \"" + windowText + "\": " + result.ToString());
			}
			result = lv_process_register_exit_callback(this.instance, ExitCallback);
			if (result != 0) {
				this.Dispose();
				throw new Exception("Failed to register exit callback.");
			}
		}
		#endregion

		#region Destructor/IDisposable
		~VenomProcess() {
			this.Dispose(false);
		}

		private bool __disposed = false;

		private void Dispose(bool disposing) {
			if (!this.__disposed) {
				if (disposing) {
					this.Exited = null;
				}

				lv_process_close(this.instance);
				Marshal.FreeHGlobal(this.instance);

				this.__disposed = true;
			}
		}

		public void Dispose() {
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion

		#region Exit Callback
		internal void ExitCallback(IntPtr instance) {
			if (this.instance != instance) {
				Log.Debug("Received callback for another instance.");
				return;
			}

			uint exit_code;

			IntPtr ptr = lv_process_t.LocationOf(this.instance, lv_process_t.Member.exit_code);
			if (ptr == IntPtr.Zero) {
				exit_code = uint.MaxValue;
			} else {
				exit_code = (uint)Marshal.ReadInt32(ptr);
			}

			this.Exited?.Invoke(this, exit_code);
		}
		#endregion

		#region Suspend/Resume/Terminate
		public void Suspend() {
			int result = lv_process_suspend(this.instance);
			if (result != 0) {
				throw new Exception("Failed to suspend process: " + result.ToString());
			}
		}

		public void Resume() {
			int result = lv_process_resume(this.instance);
			if (result != 0) {
				throw new Exception("Failed to resume process: " + result.ToString());
			}
		}

		public void Terminate(uint exit_code) {
			int result = lv_process_terminate(this.instance, exit_code);
			if (result != 0) {
				throw new Exception("Failed to terminate process: " + result.ToString());
			}
		}
		#endregion

		#region Memory
		#region MemoryRead
		public long MemoryRead(IntPtr srcAddress, byte[] dstBuffer, long dstIndex, long nbytes) {
			if (srcAddress == IntPtr.Zero) {
				throw new ArgumentNullException("Source address may not be null.");
			}

			if (dstBuffer == null) {
				throw new ArgumentNullException("Destination buffer may not be null.");
			}

			if (dstIndex < 0) {
				throw new ArgumentOutOfRangeException("Destination index may not be negative.");
			}

			if (nbytes < 0) {
				throw new ArgumentOutOfRangeException("The requested number of bytes to read may not be negative.");
			}

			if (dstBuffer.Length < nbytes) {
				throw new ArgumentException("Destination buffer is not big enough to store the requested number of bytes.");
			}

			if (nbytes == 0) {
				return 0;
			}

			int result = lv_process_memr(this.instance, srcAddress, dstBuffer, (IntPtr)nbytes, out IntPtr nRead);
			if (result != 0) {
				throw new Exception("Failed to read " + nbytes.ToString() + " bytes from " + srcAddress.ToString() + ": " + result.ToString());
			}

			return (long)nRead;
		}

		public ushort MemoryRead16(IntPtr address) {
			if (address == IntPtr.Zero) {
				throw new ArgumentNullException("Source address may not be null.");
			}

			int result = lv_process_memr16(this.instance, address, out ushort value);
			if (result != 0) {
				throw new Exception("Failed to read 16-bit value from " + address.ToString() + ": " + result.ToString());
			}

			return value;
		}

		public uint MemoryRead32(IntPtr address) {
			if (address == IntPtr.Zero) {
				throw new ArgumentNullException("Source address may not be null.");
			}

			int result = lv_process_memr32(this.instance, address, out uint value);
			if (result != 0) {
				throw new Exception("Failed to read 32-bit value from " + address.ToString() + ": " + result.ToString());
			}

			return value;
		}

		public ulong MemoryRead64(IntPtr address) {
			if (address == IntPtr.Zero) {
				throw new ArgumentNullException("Source address may not be null.");
			}

			int result = lv_process_memr64(this.instance, address, out ulong value);
			if (result != 0) {
				throw new Exception("Failed to read 64-bit value from " + address.ToString() + ": " + result.ToString());
			}

			return value;
		}
		#endregion

		#region MemoryWrite
		public long MemoryWrite(IntPtr dstAddress, byte[] srcBuffer, long srcIndex, long nbytes) {
			if (dstAddress == IntPtr.Zero) {
				throw new ArgumentNullException("Destination address may not be null.");
			}

			if (srcBuffer == null) {
				throw new ArgumentNullException("Source buffer may not be null.");
			}

			if (srcIndex < 0) {
				throw new ArgumentOutOfRangeException("Source index may not be negative.");
			}

			if (nbytes < 0) {
				throw new ArgumentOutOfRangeException("The requested number of bytes to read may not be negative.");
			}

			if (srcBuffer.Length < nbytes) {
				throw new ArgumentException("Source buffer is not big enough to obtain the requested number of bytes.");
			}

			if (nbytes == 0) {
				return 0;
			}

			int result = lv_process_memw(this.instance, dstAddress, srcBuffer, (IntPtr)nbytes, out IntPtr nWritten);
			if (result != 0) {
				throw new Exception("Failed to write " + nbytes.ToString() + " bytes to " + dstAddress.ToString() + ": " + result.ToString());
			}

			return (long)nWritten;
		}

		public void MemoryWrite16(IntPtr address, ushort value) {
			if (address == IntPtr.Zero) {
				throw new ArgumentNullException("Destination address may not be null.");
			}

			int result = lv_process_memw16(this.instance, address, value);
			if (result != 0) {
				throw new Exception("Failed to write 16-bit value to " + address.ToString() + ": " + result.ToString());
			}
		}

		public void MemoryWrite32(IntPtr address, uint value) {
			if (address == IntPtr.Zero) {
				throw new ArgumentNullException("Destination address may not be null.");
			}

			int result = lv_process_memw32(this.instance, address, value);
			if (result != 0) {
				throw new Exception("Failed to write 32-bit value to " + address.ToString() + ": " + result.ToString());
			}
		}

		public void MemoryWrite64(IntPtr address, ulong value) {
			if (address == IntPtr.Zero) {
				throw new ArgumentNullException("Destination address may not be null.");
			}

			int result = lv_process_memw64(this.instance, address, value);
			if (result != 0) {
				throw new Exception("Failed to write 64-bit value to " + address.ToString() + ": " + result.ToString());
			}
		}
		#endregion

		#region MemoryDereference
		public IntPtr MemoryDereference(IntPtr address, IntPtr offset) {
			if (address == IntPtr.Zero) {
				throw new ArgumentNullException("Source address may not be null.");
			}

			int result = lv_process_memderef(this.instance, address, offset, out IntPtr value);
			if (result != 0) {
				throw new Exception("Failed to dereference pointer located at " + address.ToString() + ": " + result.ToString());
			}

			return value;
		}

		public IntPtr MemoryDereference(IntPtr address) {
			return this.MemoryDereference(address, IntPtr.Zero);
		}

		public IntPtr MemoryDereference(IntPtr address, IntPtr[] offsets) {
			if (address == IntPtr.Zero) {
				throw new ArgumentNullException("Source address may not be null.");
			}

			if (offsets == null) {
				throw new ArgumentNullException("Offset array may not be null.");
			}

			if (offsets.Length == 0) {
				return this.MemoryDereference(address, IntPtr.Zero);
			}

			int result = lv_process_memderefmul(this.instance, address, offsets, (uint)offsets.Length, out IntPtr value);
			if (result != 0) {
				throw new Exception("Failed to dereference pointer located at " + address.ToString() + ": " + result.ToString());
			}

			return value;
		}
		#endregion
		#endregion
	}
}
