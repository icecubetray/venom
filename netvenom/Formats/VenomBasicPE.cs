﻿using NetVenom.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static NetVenom.Formats.VenomPE;

namespace NetVenom.Formats
{
	public partial class VenomBasicPE
	{
		public bool signature_file_valid = false;
		public bool signature_header_valid = false;
		public Machine machine = Machine.Unknown;
		public uint offset = 0;
		public uint timestamp = 0;

		public VenomBasicPE(byte[] data) {
			if (Memory.GetUInt16LE(data, 0) != 0x5A4D) {
				this.signature_file_valid = false;
				return;
			}
			this.signature_file_valid = true;

			this.offset = Memory.GetUInt32LE(data, 60);
			if (Memory.GetUInt32LE(data, offset) != 0x00004550) {
				this.signature_header_valid = false;
				return;
			}
			this.signature_header_valid = true;

			this.machine = (Machine)Memory.GetUInt16LE(data, (offset + 4));
			if (!Enum.IsDefined(typeof(Machine), this.machine)) {
				this.machine = Machine.Unknown;
			}

			this.timestamp = Memory.GetUInt32LE(data, (offset + 8));
		}

		public VenomBasicPE(Stream stream) {
			long original = stream.Position;

			try {
				lock (stream) {
					byte[] buffer = new byte[4];

					stream.Seek(0, SeekOrigin.Begin);
					ReadAsWife(stream, buffer, 0, 2);
					if (Memory.GetUInt16LE(buffer, 0) != 0x5A4D) {
						this.signature_file_valid = false;
						return;
					}
					this.signature_file_valid = true;
					
					stream.Seek(60, SeekOrigin.Begin);
					ReadAsWife(stream, buffer, 0, 4);
					this.offset = Memory.GetUInt32LE(buffer, 0);

					stream.Seek(this.offset, SeekOrigin.Begin);
					ReadAsWife(stream, buffer, 0, 4);
					if (Memory.GetUInt32LE(buffer, 0) != 0x00004550) {
						this.signature_header_valid = false;
						return;
					}
					this.signature_header_valid = true;

					stream.Seek(this.offset + 4, SeekOrigin.Begin);
					ReadAsWife(stream, buffer, 0, 2);
					this.machine = (Machine)Memory.GetUInt16LE(buffer, 0);
					if (!Enum.IsDefined(typeof(Machine), this.machine)) {
						this.machine = Machine.Unknown;
					}

					stream.Seek(this.offset + 8, SeekOrigin.Begin);
					ReadAsWife(stream, buffer, 0, 4);
					this.timestamp = Memory.GetUInt32LE(buffer, 0);
				}
			} finally {
				stream.Seek(original, SeekOrigin.Begin);
			}
		}

		/// <summary>
		/// Read from the specified stream, either we get it all or we complain.
		/// </summary>
		private static void ReadAsWife(Stream stream, byte[] buffer, int offset, int count) {
			// Assume parameters are valid. (Didn't even intended this part to be like a wife.)

			if (stream.Read(buffer, offset, count) != count) {
				// Complain.
				throw new IOException("Didn't read properly.");
			}
		}
	}
}
