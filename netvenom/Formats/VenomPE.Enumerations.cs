﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetVenom.Formats
{
	partial class VenomPE
	{
		public enum Machine : ushort
		{
			Unknown = 0x0000,
			x86 = 0x014C,
			x64 = 0x8664
		}
	}
}
