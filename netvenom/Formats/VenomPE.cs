﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetVenom.Formats
{
	public partial class VenomPE
	{
		public static readonly Machine CurrentMachine;

		static VenomPE() {
			if (IntPtr.Size == 8) {
				CurrentMachine = Machine.x64;
			} else if (IntPtr.Size == 4) {
				CurrentMachine = Machine.x86;
			} else {
				CurrentMachine = Machine.Unknown;
			}
		}
	}
}
